﻿using System;
using System.Text;
using DistributedAppDemo.Main.Contracts.ServiceLibrary.DTO;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace ConsoleSender
{
    class Program
    {
        // Easy way to test this: www.cloudamqp.com for a free RabbitMQ Developer server
        private static string _uri = "{place here your rabbitmq server uri}";

        private static string[] Names = new string[]
        {
            "Antonio","Pepe","Paco","Luis","Jorge","Fernando"
        };

        private static string[] SurNames = new string[]
        {
            "Fernandez", "Martinez", "Gomez", "Sanchez", "Rodriguez", "Lopez"
        };


        private static void SendMessage()
        {
            var rnd = new Random();
            var factory = new ConnectionFactory {Uri = _uri};
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "hello",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

                    var name = Names[rnd.Next(6)];
                    var surname = SurNames[rnd.Next(6)];

                    ContactDTO message = new ContactDTO
                    {
                        ContactId = Guid.NewGuid(),
                        ContactType = ContactDTO.ContactTypeEnum.Customer,
                        Email = $"{name.ToLowerInvariant()}.{surname.ToLowerInvariant()}@testdomain.com",
                        Name = name,
                        Surname = surname,
                        Company = "Microsoft"
                    };

                    var jsonMessage = JsonConvert.SerializeObject(message);

                    var body = Encoding.UTF8.GetBytes(jsonMessage);
                    
                    channel.BasicPublish(exchange: "",
                                         routingKey: "hello",
                                         basicProperties: null,
                                         body: body);
                    Console.WriteLine($" [{DateTime.Now:G}] Sent {message}");
                }
            }
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Message sender");
            ConsoleKeyInfo k;
            do
            {
                Console.Clear();
                Console.WriteLine("1 - Enviar mensaje");
                Console.WriteLine("ESC - Salir");
                k = Console.ReadKey();
                switch (k.KeyChar)
                {
                    case '1':
                        SendMessage();
                        break;
                }
            } while (k.Key != ConsoleKey.Escape);
        }
    }
}
