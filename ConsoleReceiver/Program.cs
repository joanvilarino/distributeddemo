﻿using System;
using System.Text;
using DistributedAppDemo.Main.Contracts.ServiceLibrary.DTO;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace ConsoleReceiver
{
    class Program
    {
        // Easy way to test this: www.cloudamqp.com for a free RabbitMQ Developer server
        private static string _uri = "{place here your rabbitmq server uri}";

        static void Main(string[] args)
        {
            ReceiveMessages((model, ea) =>
            {
                var body = ea.Body;
                var jsonMessage = Encoding.UTF8.GetString(body);
                var contact = JsonConvert.DeserializeObject<ContactDTO>(jsonMessage);
                Console.WriteLine($" [{DateTime.Now:T}] Received > Id: {contact.ContactId}, Name: {contact.Name} {contact.Surname}, email: {contact.Email}");
            });
        }

        private static void ReceiveMessages(Action<object, BasicDeliverEventArgs> action)
        {
            Console.WriteLine("Message Receiver - Waiting for messages");
            var factory = new ConnectionFactory { Uri = _uri };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (sender, args) => action(sender, args);
                channel.BasicConsume(queue: "hello",
                                     noAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }


    }
}
